<!--// Initialize the session-->
<?php include_once('session.php'); ?>
<div class="modal fade" id="editmodaltask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action = "edittaskaction.php  " method = "POST">
      <div class="modal-body">
                <input type = "hidden" name = "updateidtask" id="updateidtask">
        <div class="form-group">
            <label>Task Name</label>
            <input type="text" class="form-control" id="taskname" name="taskname" placeholder="Title" required>
        </div>
         <div class="form-group">
            <label>Task Description</label>
            <input type="text" class="form-control" id = "taskdes" name="taskdes" placeholder="Title" required>
        </div>
        
        <div class="form-group" disabled>
            <label>Project Task</label>
            <select class="form-control" id = "taskprojectid" name="taskprojectid" disabled>
            <option value = "select">Select</option>
            <?php
                $sql = mysqli_query($mysqli, "SELECT * from project WHERE user_id='".$_SESSION['id']."' ");
                
                $row = mysqli_num_rows($sql);
                while ($row = mysqli_fetch_array($sql)){
                echo "<option value='". $row['id'] ."'>" .$row['project_title'] ." </option>" ;
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Task Due Date</label>
            <input type="date" class="form-control" id="taskduedate" name="taskduedate" data-toggle="datepicker">
        </div>

        <div class="form-group">
            <label> Task Status</label>
            <select class="form-control" id="taskstatus" name="taskstatus" required>
            <option>Pending</option>
            <option>Ongoing</option>
            <option>Active</option>
            <option>Overdue</option>
            </select>
        </div>

        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="updatetaskdata" class="btn btn-primary">Update Task</button>
      </div>
      </form>
    </div>
  </div>
</div>


