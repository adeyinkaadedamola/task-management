<!--// Initialize the session-->
<?php include_once('session.php');
include('cleaner.php'); 
$con = mysqli_connect("localhost", "root", "","prog");

if(isset($_POST['updatetaskdata']))
{
    $id = cleanData($_POST['updateidtask']);
    
    $taskname = cleanData($_POST['taskname']);
    $taskdes = cleanData($_POST['taskdes']);
    $taskstatus = cleanData($_POST['taskstatus']);
    $taskprojectid = cleanData($_POST['taskprojectid']);
    $taskduedate = cleanData($_POST['taskduedate']);
    
    $date = str_replace('/', '-', $taskduedate );
    $newDate = date("Y-m-d", strtotime($date));
     $currentDateTime = date('Y-m-d');

    if ($newDate > $currentDateTime)
    {
        $taskstatus = 'Overdue';
    }
    else
    {
        $taskstatus = cleanData($_POST['taskstatus']);
    }

  $query = "UPDATE tasks 
  SET task_title = '".mysqli_real_escape_string($con,$taskname)."',
   task_description = '".mysqli_real_escape_string($con,$taskdes)."',
    tasks_status = '".mysqli_real_escape_string($con,$taskstatus)."',
     due_date = '".mysqli_real_escape_string($con,$newDate)."'

   WHERE id = '$id' ";
    $query_run = mysqli_query($con, $query);

    if($query_run)
    {
        echo '<script type="text/javascript">
         alert("Task updated");
        location="tasksdashboard.php";
         </script>';
    }
    else
    {
        echo 'error'. mysqli_error($con);
    }
}
