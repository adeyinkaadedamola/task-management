<!--// Initialize the session-->
<?php include_once('session.php');
include('cleaner.php'); 
$con = mysqli_connect("localhost", "root", "","prog");

if(isset($_POST['inserttaskdata']))
{
    
    $taskname = cleanData($_POST['taskname']);
    $taskdes = cleanData($_POST['taskdes']);
    $taskstatus = cleanData($_POST['taskstatus']);
    $taskprojectid = cleanData($_POST['taskprojectid']);
    $tasktid = cleanData($_POST['tasktid']);
    $taskduedate = cleanData($_POST['taskduedate']);
    
    
    $date = str_replace('/', '-', $taskduedate );
    $newDate = date("Y-m-d", strtotime($date));
    $currentDateTime = date('Y-m-d');

    if ($newDate > $currentDateTime)
    {
        $taskstatus = 'Overdue';
    }
    else
    {
        $taskstatus = strip_tags(trim($_POST['taskstatus']));;
    }

   

  $query = "INSERT INTO tasks (task_title, task_description, tasks_status, project_id, user_id, due_date) 
  VALUES ('".mysqli_real_escape_string($con,$taskname)."',
  '".mysqli_real_escape_string($con,$taskdes)."',
  '".mysqli_real_escape_string($con,$taskstatus)."',
  '".mysqli_real_escape_string($con,$taskprojectid)."',
  '".mysqli_real_escape_string($con,$tasktid)."',
  '".mysqli_real_escape_string($con,$newDate)."')";
   
    $query_run = mysqli_query($con, $query);

    if($query_run)
    {
       echo '<script type="text/javascript">
         alert("Task Added");
        location="tasksdashboard.php";
         </script>';
    }
    else
    {
        echo 'error'. mysqli_error($con);
    }
}