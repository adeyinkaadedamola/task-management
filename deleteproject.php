<!--// Initialize the session-->
<?php include_once('session.php'); ?>
<div class="modal fade" id="deleteproject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Project Data </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action = "deleteprojectaction.php" method = "POST">
      <div class="modal-body"> 
        <input type = "hidden" name = "deleteid" id="deleteid">
        <h4>Are you sure?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">NO || NEVER MIND</button>
        <button type="submit" name="deletedata" class="btn btn-primary">YES || DELETE IT</button>
      </div>
        </form>
    </div>
  </div>
</div>