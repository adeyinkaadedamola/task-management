<?php include_once('head.php'); ?>

<!--// Initialize the session-->
<?php include_once('session.php'); ?>

<div class="page-header">
        <h1>Hello,<b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> Your Project Page</h1>
        <p>
        <a href="logout.php" class="btn btn-danger">Logout</a>
    </p>
</div>
            <div class="card">
                <div class = "card-body">
                
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addprojectmodal">Add Project <span class="badge badge-light">
                    <?php
                        $sql = mysqli_query($mysqli, "SELECT * from project WHERE user_id='".$_SESSION['id']."' ");
                        $number = mysqli_num_rows($sql);
                    echo $number;
                     ?>
                    </span></button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskmodal">Add Task<span class="badge badge-light">
                    <?php
                        $sql = mysqli_query($mysqli, "SELECT * from tasks WHERE user_id='".$_SESSION['id']."' ");
                        $number = mysqli_num_rows($sql);
                    echo $number;
                     ?>
                    </span></button>
                    <a class="btn btn-primary" href="alltasks.php" role="button">Tasks</a>
                </div>
            </div>
    
<?php include_once('projecttable.php'); ?>
<?php include_once('addproject.php'); ?>
<?php include_once('editproject.php'); ?>
<?php include_once('deleteproject.php'); ?>
<?php include_once('addtask.php'); ?>


<!--Sripts-->
<script>
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="js/datepicker.js"></script>
<!--Edit project script-->
<script>
    $(document).ready(function(){
        $('.editbtn').on('click', function(){
            $('#editmodal').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#updateid').val(data[0]);
            $('#projectname').val(data[1]);
            $('#projectdes').val(data[2]);
            $('#projectduedate').val(data[4]);
            $('#projectstatus').val(data[5]);

        });

    });
</script>
<!--Delete Project Script-->
<script>
    $(document).ready(function(){
        $('.deleteprojectbtn').on('click', function(){
            $('#deleteproject').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#deleteid').val(data[0]);

        });

    });
</script>


<!--Datepicker-->
<script>
    $(function() {
      $('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        zIndex: 2048,
      });
    });
  </script>

<!--DataTables-->
<script>
$(document).ready(function() {
    $('#datatabbleid').DataTable();
} );
</script>

<?php include_once('footer.php'); ?>