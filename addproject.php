<!--// Initialize the session-->
<?php include_once('session.php'); ?>

<div class="modal fade" id="addprojectmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action = "addprojectaction.php  " method = "POST">
      <div class="modal-body">
      <input type="hidden" id="projid" name="projid" value="<?php echo htmlspecialchars($_SESSION["id"]); ?>">  
        <div class="form-group">
            <label>Project Name</label>
            <input type="text" class="form-control" name="projectname" placeholder="Title" required>
        </div>
         <div class="form-group">
            <label>Project Description</label>
            <input type="text" class="form-control" name="projectdes" placeholder="Title" required>
        </div>

        <div class="form-group">
            <label>Project Due Date</label>
            <input type="date" id="date" class="form-control" name="projectduedate">
        </div>
        
        <div class="form-group">
            <label>Status</label>
            <select class="form-control" name="projectstatus" required>
            <option>Pending</option>
            <option>Ongoing</option>
            <option>Active</option>
            <option>Overdue</option>
            </select>
        </div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="insertdata" class="btn btn-primary">Save Project</button>
      </div>
        </form>
    </div>
  </div>
</div>

