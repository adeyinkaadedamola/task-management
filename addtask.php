<!--// Initialize the session-->
<?php include_once('session.php'); ?>
<div class="modal fade" id="addtaskmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action ="addtaskaction.php" method = "POST">
      <div class="modal-body">
       <input type="hidden" id="tasktid" name="tasktid" value="<?php echo htmlspecialchars($_SESSION["id"]); ?>"> 
        <div class="form-group">
            <label>Task Name</label>
            <input type="text" class="form-control" name="taskname"  placeholder="Title" required>
        </div>
         <div class="form-group">
            <label>Task Description</label>
            <input type="text" class="form-control" name="taskdes" placeholder="Title" required>
        </div>
        
        <div class="form-group">
            <label>Project Task</label>
            <select class="form-control" name="taskprojectid" required>
            <option value = "select"></option>
            <?php
                $sql = mysqli_query($mysqli, "SELECT * from project WHERE user_id='".$_SESSION['id']."' ");
                
                $row = mysqli_num_rows($sql);
                while ($row = mysqli_fetch_array($sql)){
                echo "<option value='". $row['id'] ."'>" .$row['project_title'] ." </option>" ;
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Task Due Date</label>
            <input type="date" id="date" class="form-control" name="taskduedate">
        </div>

         <div class="form-group">
            <label> Task Status</label>
            <select class="form-control" name="taskstatus" required>
            <option>Pending</option>
            <option>Ongoing</option>
            <option>Active</option>
            <option>Overdue</option>
            </select>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="inserttaskdata" class="btn btn-primary">add Task</button>
      </div>
      </form>
    </div>
  </div>
</div>