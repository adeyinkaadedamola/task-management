<?php include ("config.php"); ?>
<?php include_once('session.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
  
  
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
        
    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse visible-xs">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Task Manager</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="dashboard.php">Dashboard</a></li>
        <li><a href="projectdashboard.php">Projects</a></li>
        <li><a href="tasksdashboard.php">Tasks</a></li>
        <li><a href="logout.php" id="logout">Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content">
        <div class="col-sm-3 sidenav hidden-xs">
        <h2>Task Manager</h2>
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="dashboard.php">Dashboard</a></li>
            <li><a href="projectdashboard.php">Projects</a></li>
            <li><a href="tasksdashboard.php">Tasks</a></li>
            <li><a href="logout.php" id="logout">Logout</a></li>
        </ul><br>
        </div>
    
    
    <div class="col-sm-9">
      <div class="well">
        <h3>Hi,<b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> Welcome to your Dashboard</h3>
      </div>
    <div class="row">
        <div class="col-sm-3">
          <div class="well">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addprojectmodal">Add Project </button>
            <h4><span class="badge badge-light">
                    <?php
                        $sql = mysqli_query($mysqli, "SELECT * from project WHERE user_id='".$_SESSION['id']."' ");
                        $number = mysqli_num_rows($sql);
                    echo $number;
                     ?>
            </span></h4>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="well">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskmodal">Add Task</button>
            <h4><span class="badge badge-light">
                    <?php
                        $sql = mysqli_query($mysqli, "SELECT * from tasks WHERE user_id='".$_SESSION['id']."' ");
                        $number = mysqli_num_rows($sql);
                    echo $number;
                     ?>
                </span></h4>
          </div>
        </div> 
    
      

      <div class="row">
        <?php include_once('recentprojecttable.php'); ?>
        <?php include_once('addproject.php'); ?>
        <?php include_once('editproject.php'); ?>
        <?php include_once('deleteproject.php'); ?>
        <?php include_once('recenttasktable.php'); ?>
        <?php include_once('addtask.php'); ?>
        <?php include_once('edittask.php'); ?>
        <?php include_once('deletetask.php'); ?>
      </div>
  </div>
</div>    
   

<?php include('scripts.php'); ?>
<?php include_once('footer.php'); ?>
