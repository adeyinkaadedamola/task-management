<!--// Initialize the session-->
<?php include_once('session.php'); ?>
<?php $con = mysqli_connect("localhost", "root", "","prog");
?>
<style> 
td.hidden,th.hidden {
    display:none;
}
</style>
<div class = "container">

    <div class="jumbotron">
 
    <div class="card">
                <h2>Your Projects</h2>
            </div>
        
            <div class ="card">
                <div class = "card-body">
                    <?php
                        $query = "SELECT * FROM project WHERE user_id='".$_SESSION['id']."' ORDER BY time_created DESC";
                        $query_run = mysqli_query($con, $query);  
                    ?>
                        <table id="datatableid" class="table">
                        <thead>
                            <tr>
                            <th class="hidden" scope="col">Hidden id</th>
                            <th scope="col">Project Title</th>
                            <th scope="col">Project Description</th>
                            <th scope="col">Project Start Date</th>
                            <th scope="col">Project Due Date</th>
                            <th scope="col">Project status</th>
                            <th scope="col">Add Tasks</th>
                            <th scope="col">EDIT</th>
                            <th scope="col">DELETE</th>
                            </tr>
                        </thead>

                     <?php
                        if($query_run)
                        {
                            foreach($query_run as $row)
                            {  
                    ?>
                        <tbody>
                            <tr>
                            <td class="hidden"><?php echo $row['id']; ?></td>
                            <td><?php echo $row['project_title']; ?></td>
                            <td><?php echo $row['description']; ?></td>
                            <td><?php echo $row['time_created']; ?></td>
                            <td><?php echo $row['due_date']; ?></td>
                            <td><?php echo $row['status']; ?></td>
                            <td> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskmodal">Add Task</td>
                            <td> <button type = "button" class = "btn btn-success editbtn"> EDIT  </td>
                            <td> <button type = "button" class = "btn btn-danger deleteprojectbtn"> DELETE  </td>
                            </tr>
                        </tbody>
                            
                    <?php
                         }
                        }
                        else
                        {
                            echo "No Record Found";
                        }
                    ?>     
                        </table>
                </div>
            </div>
</div>

