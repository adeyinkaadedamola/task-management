<!--Scripts-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>



<!--Edit project script-->
<script>
    $(document).ready(function(){
        $('.editbtn').on('click', function(){
            $('#editmodal').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#updateid').val(data[0]);
            $('#projectname').val(data[1]);
            $('#projectdes').val(data[2]);
            $('#projectduedate').val(data[4]);
            $('#projectstatus').val(data[5]);

        });

    });
</script>
<!--Delete Project Script-->
<script>
    $(document).ready(function(){
        $('.deleteprojectbtn').on('click', function(){
            $('#deleteproject').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#deleteid').val(data[0]);

        });

    });
</script>
<!--Delete Task Script-->
<script>
    $(document).ready(function(){
        $('.deletetaskbtn').on('click', function(){
            $('#deletetask').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#deletetid').val(data[0]);

        });

    });
</script>
<!--Edit Task script-->
<script>
      $(document).ready(function(){
        $('.editbtntask').on('click', function(){
            $('#editmodaltask').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#updateidtask').val(data[0]);
            $('#taskname').val(data[1]);
            $('#taskdes').val(data[2]);
            $('#taskprojectid').val(data[3])
            $('#taskstatus').val(data[4]);
            $('#taskduedate').val(data[6]);

        });
     });
</script>
<!--DataTables-->
<script>
$(document).ready(function() {
    $('#datatableid').DataTable({
        "pagingtype": "full_numbers",
        "lengthMenu": [
                         [10, 25, 50, -1],
                        [10, 25, 50, "ALL"]
        ],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search",
        }
    });
} );
</script>



<!--Logout confirmation-->
<script>
$(function(){
    $('a#logout').click(function(){
        if(confirm('Are you sure to logout')) {
            return true;
        }

        return false;
    });
});
</script>
