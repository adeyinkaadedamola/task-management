<?php include_once('head.php'); ?>

<!--// Initialize the session-->
<?php include_once('session.php'); ?>

<div class="page-header">
        <h1>Hello,<b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> Your Tasks Page</h1>
        <p>
        <a href="logout.php" class="btn btn-danger">Logout</a>
    </p>
</div>
            <div class="card">
                <div class = "card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskmodal">Add Task<span class="badge badge-light">
                    <?php
                        $sql = mysqli_query($mysqli, "SELECT * from tasks WHERE user_id='".$_SESSION['id']."' ");
                        $number = mysqli_num_rows($sql);
                    echo $number;
                     ?>
                    </span></button>
            </div>
    
<?php include_once('tasktable.php'); ?>
<?php include_once('addtask.php'); ?>
<?php include_once('edittask.php'); ?>
<?php include_once('deletetask.php'); ?>


<!--Sripts-->
<script>
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="js/datepicker.js"></script>
<!--Delete Task Script-->
<script>
    $(document).ready(function(){
        $('.deletetaskbtn').on('click', function(){
            $('#deletetask').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#deletetid').val(data[0]);

        });

    });
</script>
<!--Edit Task script-->
<script>
      $(document).ready(function(){
        $('.editbtntask').on('click', function(){
            $('#editmodaltask').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function(){
                return  $(this).text();
            }).get()

            console.log(data);
            $('#updateidtask').val(data[0]);
            $('#taskname').val(data[1]);
            $('#taskdes').val(data[2]);
            $('#taskprojectid').val(data[3])
            $('#taskstatus').val(data[4]);
            $('#taskduedate').val(data[6]);

        });
     });
</script>


<!--Datepicker-->
<script>
    $(function() {
      $('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        zIndex: 2048,
      });
    });
  </script>

<!--DataTables-->
<script>
$(document).ready(function() {
    $('#datatabbleid').DataTable();
} );
</script>

<?php include_once('footer.php'); ?>