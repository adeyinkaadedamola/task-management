<?php
function cleanData($data) {

     $data = htmlspecialchars($data);
     $data = strip_tags($data);
     $data = stripslashes($data);
     $data = trim($data);
    return $data;

   }
   ?>