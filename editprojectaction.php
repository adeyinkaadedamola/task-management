<!--// Initialize the session-->
<?php include_once('session.php');
include('cleaner.php'); 
$con = mysqli_connect("localhost", "root", "","prog");

if(isset($_POST['updatedata']))
{
 
    $id = $_POST['updateid'];

    $projectname = cleanData($_POST['projectname']);
    $projectdes = cleanData($_POST['projectdes']);
    $projectstatus = cleanData($_POST['projectstatus']);
    $projectduedate = cleanData($_POST['projectduedate']);
    
    $date = str_replace('/', '-', $projectduedate );
    $newDate = date("Y-m-d", strtotime($date));
    $currentDateTime = date('Y-m-d');

    if ($newDate > $currentDueDate)
    {
        $projectstatus = 'Overdue';
    }
    else
    {
        $projectstatus = cleanData($_POST['projectstatus']);
    }

  $query = "UPDATE project
   SET project_title = '".mysqli_real_escape_string($con,$projectname)."',
    description = '".mysqli_real_escape_string($con,$projectdes)."',
     status = '".mysqli_real_escape_string($con,$projectstatus)."',
      due_date = '".mysqli_real_escape_string($con,$newDate)."'
       WHERE id = '$id' ";
    $query_run = mysqli_query($con, $query);

    if($query_run)
    {
        echo '<script type="text/javascript">
         alert("Project Updated");
        location="projectdashboard.php";
         </script>';
    }
    else
    {
        echo 'error'. mysqli_error($con);
    }
}