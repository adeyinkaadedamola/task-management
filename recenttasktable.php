<!--// Initialize the session-->
<?php include_once('session.php'); ?>
<?php $con = mysqli_connect("localhost", "root", "","prog");?>
<style> 
td.hidden,th.hidden {
    display:none;
}
</style>
<div class = "container">
 
    <div class="jumbotron">
       

            <div class ="card">
            <h2>Your Most Recent Tasks</h2>
                <div class = "card-body">
                    <?php
                        
                        $query = "SELECT tasks.id, tasks.task_title, tasks.task_description, project.project_title, tasks.tasks_status, tasks.time_created, tasks.due_date
                         FROM tasks
                         INNER JOIN project 
                         ON tasks.project_id=project.id 
                         WHERE project.user_id='".$_SESSION['id']."'
                         ORDER BY project.time_created
                         DESC LIMIT 10";
                        $query_run = mysqli_query($con, $query); 
                    ?>
                        <table class="table">
                        <thead>
                            <tr>
                            <th class="hidden" scope="col">Hidden id</th>
                            <th scope="col">Task Title</th>
                            <th scope="col">Task Description</th>
                            <th scope="col">Project</th>
                            <th scope="col">Task status</th>
                            <th scope="col">Task Start Date</th>
                            <th scope="col">Task Due Date</th>
                            <th scope="col">EDIT</th>
                            <th scope="col">DELETE</th>
                            </tr>
                        </thead>
                        
                     <?php
                        if($query_run)
                        {
                            foreach($query_run as $row)
                            {       
                    ?>
                        <tbody>

                            <tr>
                            <td class="hidden"><?php echo $row['id'];?></td>
                            <td><?php echo $row['task_title']; ?></td>
                            <td><?php echo $row['task_description']; ?></td>
                            <td><?php echo $row['project_title']; ?></td>
                            <td><?php echo $row['tasks_status']; ?></td>
                            <td><?php echo $row['time_created']; ?></td>
                            <td><?php echo $row['due_date']; ?></td>
                            <td> <button type = "button" class = "btn btn-success editbtntask"> EDIT  </td>
                            <td> <button type = "button" class = "btn btn-danger deletetaskbtn"> DELETE  </td>
                            </tr>
                        </tbody>

                    <?php
                         }
                        }
                        else 
                        {
                            echo 'No Task Found';
                        }
                    ?>     
                        </table>
                </div>
            </div>
    </div>        
</div>
