

<?php
include ("config.php");
?>

<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: adlogin.php");
    exit;
}
?>


<!DOCTYPE html>
<html lang="en">
<head> 
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="page-header">
        <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Welcome to your Dashboard</h1>
    </div>


    <p>
        <a href="logout.php" class="btn btn-danger">Logout</a>
    </p>

    <div class = "container">
        <div class="jumbotron">
            <div class="card">
                <h2>Projects</h2>
            </div>
            <div class="card">
                <div class = "card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addprojectmodal">Projects <span class="badge badge-light">
                    <?php
                        $sql = mysqli_query($mysqli, "SELECT * from project");
                        $number = mysqli_num_rows($sql);
                    echo $number;
                     ?>
                    </span></button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskmodal">Tasks<span class="badge badge-light">
                    <?php
                        $sql = mysqli_query($mysqli, "SELECT * from tasks");
                        $number = mysqli_num_rows($sql);
                    echo $number;
                     ?>
                    </span></button>
                </div>
            </div>

    <div class ="card">
                <div class = "card-body">
                    <?php
                        $connection = mysqli_connect("localhost", "root", "");
                        $db = mysqli_select_db($connection, "prog");
                        $query = "SELECT project.project_title, project.description, users.username, project.status, project.time_created 
                        FROM project 
                        INNER JOIN users 
                        ON project.user_id=users.id 
                        ORDER BY users.username";
                        $query_run = mysqli_query($connection, $query);  
                    ?>
                        <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Project Title</th>
                            <th scope="col">Project Description</th>
                            <th scope="col">Project Owner</th>
                            <th scope="col">Project status</th>
                            <th scope="col">Project Start Date</th>
                        </thead>

                     <?php
                        if($query_run)
                        {
                            foreach($query_run as $row)
                            {       
                    ?>
                        <tbody>

                            <tr>
                            <td><?php echo $row['project_title']; ?></td>
                            <td><?php echo $row['description']; ?></td>
                            <td><?php echo $row['username']; ?></td>
                            <td><?php echo $row['status']; ?></td>
                            <td><?php echo $row['time_created']; ?></td>
                            </tr>
                        </tbody>

                    <?php
                         }
                        }
                        else
                        {
                            echo 'No project found';
                        }
                    ?>     
                        </table>
                </div>
            </div>
<!--#################################################################################################################################################################################################################3-->    
<!--TASK TABLE-->       
            <div class="card">
                <h2>Your Tasks</h2>
            </div>

            <div class ="card">
                <div class = "card-body">
                    <?php
                        $connection = mysqli_connect("localhost", "root", "");
                        $db = mysqli_select_db($connection, "prog");
                        
                        $query = "SELECT tasks.task_title, tasks.task_description, project.project_title, tasks.tasks_status, tasks.time_created FROM tasks INNER JOIN project ON tasks.project_id=project.id ORDER BY project.project_title";
                        $query_run = mysqli_query($connection, $query);  
                    ?>
                        <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Task Title</th>
                            <th scope="col">Task Description</th>
                            <th scope="col">Project </th>
                            <th scope="col">Task status</th>
                            <th scope="col">Task Start Date</th>
                        </thead>

                     <?php
                        if($query_run)
                        {
                            foreach($query_run as $row)
                            {       
                    ?>
                        <tbody>

                            <tr>
                            <td><?php echo $row['task_title']; ?></td>
                            <td><?php echo $row['task_description']; ?></td>
                            <td><?php echo $row['project_title']; ?></td>
                            <td><?php echo $row['tasks_status']; ?></td>
                            <td><?php echo $row['time_created']; ?></td>
                            </tr>
                        </tbody>

                    <?php
                         }
                        }
                        else 
                        {
                            echo 'error'. mysqli_error($connection);
                        }
                    ?>     
                        </table>
                </div>
            </div>


        </div> 
    </div>

</body>
</html>
